package fr.formation.twitterxs;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.formation.twitterxs.dtos.UserCreateDto;
import fr.formation.twitterxs.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

	@Autowired
	UserService service;
	
	
	@Test
	public void testEmailExistant() {
		
		String email = (Math.random()*1000)+"testJunit@blabla.com";
		
		UserCreateDto user = new UserCreateDto();
		user.setBirthDate(LocalDate.now().minusYears(20));
		user.setEmail(email);
		user.setFirstname("J");
		user.setLastname("unit");
		user.setRegionId(1);
		service.save(user);
		
		Assert.assertTrue(service.emailAlreadyExists(email));
	}
}
