package fr.formation.twitterxs.entities;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = 
		{"author_id" , "postDate"}))
public class Tweet {

	public Tweet() {}
	
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long id;
		
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(nullable = false)
	private User author;
	
	@Column(nullable = false , updatable = false)
	private LocalDateTime postDate;
	
	@Column(nullable = false , updatable = false)
	private LocalDateTime editDate;
	
	@Column(length = 280 , nullable = false)
	private String content;

	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public LocalDateTime getPostDate() {
		return postDate;
	}

	public void setPostDate(LocalDateTime postDate) {
		this.postDate = postDate;
	}

	public LocalDateTime getEditDate() {
		return editDate;
	}

	public void setEditDate(LocalDateTime editDate) {
		this.editDate = editDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
	//Methode Equal + HashCode autogenéré par eclipse
	/*
	@Override
	public int hashCode() {
		return Objects.hash(author, postDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Tweet)) {
			return false;
		}
		Tweet other = (Tweet) obj;
		return Objects.equals(author, other.author) && Objects.equals(postDate, other.postDate);
	}
	*/


	//Methode Equal + HashCode Frank (plus rapide) 
	@Override
	public int hashCode() {
		return Objects.hash(author,postDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if(!(obj instanceof Tweet)) {
			return false;
		}
		Tweet oth = (Tweet) obj;
		return (author.equals(oth.getAuthor()) && postDate.equals(oth.getPostDate()));
	};

	
}
