package fr.formation.twitterxs.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 100 , nullable = false)
	private String firstname;

	@Column(length = 100 , nullable = false)
	private String lastname;
	
	@Column(nullable = false, unique=true)
	private String email;
	
	@Column(nullable = false)
	private LocalDate birthDate;
	
	@Column(nullable = false , updatable=false)
	private LocalDateTime subscriptionTime;
	
	@ManyToOne
	@JoinColumn(nullable = false)
	private Region region;

	@Embedded
	UserSecurity userSecurity;
	
	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY , cascade = CascadeType.REMOVE)
	private Set<Tweet> tweets;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDateTime getSubscriptionTime() {
		return subscriptionTime;
	}

	public void setSubscriptionTime(LocalDateTime subscriptionTime) {
		this.subscriptionTime = subscriptionTime;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public UserSecurity getUserSecurity() {
		return userSecurity;
	}

	public void setUserSecurity(UserSecurity userSecurity) {
		this.userSecurity = userSecurity;
	}

	public Set<Tweet> getTweets() {
		return tweets;
	}

	public void setTweets(Set<Tweet> tweets) {
		this.tweets = tweets;
	}
	
}
