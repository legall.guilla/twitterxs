package fr.formation.twitterxs.entities;


import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserSecurity {


	@Column(length=50, nullable = false, unique = true , updatable = false)
	private String username;
	
	@Column(length=100, nullable = false)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
