package fr.formation.twitterxs.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.twitterxs.dtos.TweetDto;
import fr.formation.twitterxs.entities.Tweet;


public interface TweetJpaRepository extends JpaRepository<Tweet, Long>{

	//JPQL query
	@Query("select new fr.formation.twitterxs.dtos.TweetDto (t.id,t.postDate,t.content) "
			+ "from Tweet t where t.author.userSecurity.username =:username order by t.postDate desc ")
	public Page<TweetDto> findbyUsername(@Param("username")String username, Pageable pageable);

}
