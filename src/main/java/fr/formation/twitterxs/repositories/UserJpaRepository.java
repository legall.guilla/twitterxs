package fr.formation.twitterxs.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.twitterxs.dtos.UserDto;
import fr.formation.twitterxs.entities.User;

public interface UserJpaRepository  extends JpaRepository<User, Long>{

	//Requete derivé
	List<User> findAllByFirstnameContainingIgnoreCase(String firstname);

	//Requete jpql
	@Query("select new fr.formation.twitterxs.dtos.UserDto(u.id, u.firstname , u.lastname , r.country ) "
			+ "from User u join u.region r")
	List<UserDto> findAllUserDto();
	
	@Query("select new fr.formation.twitterxs.dtos.UserDto(u.id, u.firstname , u.lastname , r.country ) "
			+ "from User u join u.region r where u.firstname LIKE %:firstname%")
	List<UserDto> findAllUserDtoLikeFirstName(@Param("firstname") String firstname);

	boolean existsByEmailIgnoreCase(String email);
	

}
