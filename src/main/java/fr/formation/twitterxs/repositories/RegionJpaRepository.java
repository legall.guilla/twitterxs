package fr.formation.twitterxs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.twitterxs.entities.Region;

public interface RegionJpaRepository extends JpaRepository<Region, Long>{

	Region findByCountry(String country);

	
}
