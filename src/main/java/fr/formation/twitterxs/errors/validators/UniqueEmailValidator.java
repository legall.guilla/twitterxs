package fr.formation.twitterxs.errors.validators;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import fr.formation.twitterxs.services.UserService;

public class UniqueEmailValidator  implements ConstraintValidator<UniqueEmail, String>{

	@Autowired
	UserService userService;
	
	@Override
	public boolean isValid(String email, ConstraintValidatorContext context) {

		return (!userService.emailAlreadyExists(email));
	}

}
