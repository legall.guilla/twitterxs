package fr.formation.twitterxs.errors.validators;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AdultValidator implements ConstraintValidator<Adult, LocalDate>{

	private int age;
	
	protected AdultValidator() {
		// empty
	}
	
	@Override
	public void initialize(Adult parameters) {
		age = parameters.age();
		if(age< 0) {
			throw new IllegalArgumentException("age must be positive");
		}
	}

	@Override
	public boolean isValid(LocalDate birthdate, ConstraintValidatorContext context) {
		if(null == birthdate) {
			return true;
		}
		
		return LocalDate.now().minusYears(age).isAfter(birthdate);
	}

}
