package fr.formation.twitterxs.services;

import java.util.List;

import fr.formation.twitterxs.entities.Region;

public interface RegionService {

	public Region save(Region region);

	public List<Region> list();

	Region getByCountry(String countryName);

}
