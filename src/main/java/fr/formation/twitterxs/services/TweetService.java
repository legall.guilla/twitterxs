package fr.formation.twitterxs.services;

import javax.validation.Valid;

import fr.formation.twitterxs.dtos.SearchResultDto;
import fr.formation.twitterxs.dtos.TweetCreateDto;
import fr.formation.twitterxs.dtos.TweetDto;
import fr.formation.twitterxs.dtos.TweetSearchDto;

public interface TweetService {

	public void create(@Valid TweetCreateDto tweetDto);

	public SearchResultDto<TweetDto> feed(TweetSearchDto dto);
}
