package fr.formation.twitterxs.services;

import java.util.List;

import fr.formation.twitterxs.dtos.UserCreateDto;
import fr.formation.twitterxs.dtos.UserDto;
import fr.formation.twitterxs.entities.User;

public interface UserService {

	List<UserDto> list();

	List<UserDto> listByName(String name);

	Long save(UserCreateDto dto);

	Boolean delete(Long id);

	boolean emailAlreadyExists(String email);
}
