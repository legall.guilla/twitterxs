package fr.formation.twitterxs.services.impl;

import org.springframework.stereotype.Service;

import fr.formation.twitterxs.services.TestService;

@Service
public class TestServiceImpl implements TestService {

	@Override
	public String getWeekday(String day) {
		String result = "";
		switch (day) {
		case "Lundi":
			result = "Nous sommes Lundi";
			break;
		case "Mardi":
			result = "Nous sommes Mardi";
			break;
		case "Mercredi":
			result = "Nous sommes Mercredi";
			break;
		case "Jeudi":
			result = "Nous sommes Jeudi";		
			break;
		case "Vendredi":
			result = "Nous sommes Vendredi";
			break;
		case "Samedi":
			result = "Nous sommes Samedi";
			break;
		case "Dimanche":
			result = "Nous sommes Dimanche";
			break;
		default:
			result = "erreur";
			break;
		}
		return result;
	}

}
