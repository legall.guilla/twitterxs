package fr.formation.twitterxs.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import fr.formation.twitterxs.dtos.UserCreateDto;
import fr.formation.twitterxs.dtos.UserDto;
import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.entities.User;
import fr.formation.twitterxs.repositories.RegionJpaRepository;
import fr.formation.twitterxs.repositories.UserJpaRepository;
import fr.formation.twitterxs.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserJpaRepository repo;
	
	@Autowired
	RegionJpaRepository regionRepo;
	
	@Autowired
	ModelMapper mapper;
	
	@Override
	public List<UserDto> list() {
		
		return repo.findAllUserDto();
		
	}

	@Override
	public List<UserDto> listByName(String name) {
		//return repo.findAllByFirstnameContainingIgnoreCase(name);
		return repo.findAllUserDtoLikeFirstName(name);
	}

	@Override
	public Long save(UserCreateDto dto) {
		User user = mapper.map(dto, User.class);
		
		Region region = regionRepo.getOne(dto.getRegionId());
		user.setRegion(region);
		
		user.setSubscriptionTime(LocalDateTime.now());
		
		User result = repo.save(user);
		return result.getId();
	}

	@Override
	public Boolean delete(Long id) {
		try {
			repo.deleteById(id);
		}catch(Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean emailAlreadyExists(String email) {
		return repo.existsByEmailIgnoreCase(email);
	}

}
