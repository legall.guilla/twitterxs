package fr.formation.twitterxs.services.impl;

import java.time.LocalDateTime;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.formation.twitterxs.dtos.SearchResultDto;
import fr.formation.twitterxs.dtos.TweetCreateDto;
import fr.formation.twitterxs.dtos.TweetDto;
import fr.formation.twitterxs.dtos.TweetSearchDto;
import fr.formation.twitterxs.entities.Tweet;
import fr.formation.twitterxs.entities.User;
import fr.formation.twitterxs.repositories.TweetJpaRepository;
import fr.formation.twitterxs.repositories.UserJpaRepository;
import fr.formation.twitterxs.services.TweetService;

@Service
public class TweetServiceImpl implements TweetService {

	@Autowired
	TweetJpaRepository tweetRepo;
	
	@Autowired
	UserJpaRepository userRepo;
	
	@Autowired
	ModelMapper mapper;
	
	@Override
	public void create(TweetCreateDto tweetdto) {
		Tweet tweet = mapper.map(tweetdto, Tweet.class);
		System.out.println("id du tweet ="+tweet.getId());
		User user = userRepo.getOne(tweetdto.getAuthorId());
		tweet.setAuthor(user);
		
		tweet.setPostDate(LocalDateTime.now());
		tweet.setEditDate(LocalDateTime.now());
		tweetRepo.save(tweet);
	}

	@Override
	public SearchResultDto<TweetDto> feed(TweetSearchDto dto) {
		Pageable pageable = PageRequest.of(dto.getPage(), dto.getSize());
		Page<TweetDto> page = tweetRepo.findbyUsername(dto.getUsername(),pageable);
		return new SearchResultDto<>(page.getContent(),page.getTotalElements());
	}

}
