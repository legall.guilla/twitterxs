package fr.formation.twitterxs.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.repositories.RegionJpaRepository;
import fr.formation.twitterxs.services.RegionService;

@Service
public class RegionServiceImpl implements RegionService{

	@Autowired
	private RegionJpaRepository repo;
	
	@Override
	public Region save(Region region) {
		return repo.save(region);
	}
	
	@Override
	public Region getByCountry(String countryName) {
		Region result = repo.findByCountry(countryName);
		return result;
	}

	@Override
	public List<Region> list() {
		return repo.findAll();
	}

}
