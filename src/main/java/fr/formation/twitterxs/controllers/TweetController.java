package fr.formation.twitterxs.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.dtos.SearchResultDto;
import fr.formation.twitterxs.dtos.TweetCreateDto;
import fr.formation.twitterxs.dtos.TweetDto;
import fr.formation.twitterxs.dtos.TweetSearchDto;
import fr.formation.twitterxs.entities.Tweet;
import fr.formation.twitterxs.services.TweetService;

@RestController
@RequestMapping("/tweets")
public class TweetController extends BaseController {

	@Autowired
	TweetService service;
	
	@PostMapping("/save")
	public void save(@Valid @RequestBody TweetCreateDto tweetDto) {
		service.create(tweetDto);
	}
	
	@GetMapping("/feed/{username}")
	public SearchResultDto<TweetDto> feed(
			@PathVariable("username") String username,
			@RequestParam(name = "page", required = false) Integer page, 
			@RequestParam(name = "size", required = false) Integer size ){
		
		int pageEnCours = (null == page ? 0 : Integer.max(0, page));
		int sizeEnCours = (null == size ? 5 : Integer.max(1, size));
		
		TweetSearchDto dto = new TweetSearchDto(username,pageEnCours,sizeEnCours);
		return service.feed(dto);
	}
}
