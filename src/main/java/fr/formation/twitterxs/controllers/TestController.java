package fr.formation.twitterxs.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.dto.TestDto;
import fr.formation.twitterxs.services.TestService;

@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

	
	@Autowired
	//@Qualifier("nom")
	private TestService service;
	
	/*
	@Autowired
	public public TestController(TestService service) {
		this.service = service;
	}
	*/
	
	@GetMapping("/hello")
	public String hello() {
		return "{\"value\":\"Hello Spring boot!\"}";
		//return "Hello spring boot webservice";
	}
	
	
	@GetMapping("/weekdays")
	protected List<String> weekdays(){
		return Arrays.asList("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche");
	}
	
	@GetMapping("/weekday/{day}")
	protected String weekday(@PathVariable("day") String day ) {
		return service.getWeekday(day);
	}
	
	@GetMapping("/weekday/")
	protected String weekdayParam(@RequestParam("abbr") String abbr) {
		return service.getWeekday(abbr);
	}
	
	
	@PostMapping("/save")
	protected void weekdayParam(@RequestBody TestDto dto) {
		System.out.println(dto.toString());
	}
	
	
}
