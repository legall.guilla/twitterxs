package fr.formation.twitterxs.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.entities.Region;
import fr.formation.twitterxs.services.RegionService;

@RestController
@RequestMapping("/regions")
public class RegionController extends BaseController {

	
	@Autowired
	private RegionService service;
	
	@PostMapping("/save")
	protected Region save(@RequestBody Region region) {
		System.out.println(region);
		return service.save(region);
	}
	
	@GetMapping("/list")
	protected List<Region> list() {
		List<Region> result = service.list();
		return result;
	}
	
	@GetMapping("/country/{name}")
	protected Region getByCountryName(@PathVariable("name") String name){
		return service.getByCountry(name);
	}
}
