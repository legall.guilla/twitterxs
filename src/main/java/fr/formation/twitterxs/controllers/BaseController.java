package fr.formation.twitterxs.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.formation.twitterxs.errors.response.ApiErrors;
import fr.formation.twitterxs.errors.response.MessageNotReadableError;
import fr.formation.twitterxs.errors.response.ValidationError;

public abstract class BaseController extends ResponseEntityExceptionHandler {

	 /**
     * Convenient method returning the request URI.
     *
     * @return the request URI
     */
    protected static String getRequestURI() {
    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
        .currentRequestAttributes();
    return attr.getRequest().getRequestURI();
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	List<FieldError> fieldsErrors = ex.getBindingResult().getFieldErrors();
    	List<ValidationError> errors =new ArrayList<>(fieldsErrors.size());
    	ValidationError error = null;
    	for (FieldError fieldError : fieldsErrors) {
    		String entityName = fieldError.getObjectName();
    		String fieldName = fieldError.getField();
    		String errCode = fieldError.getDefaultMessage();
    		error = ValidationError.ofFieldType(entityName, fieldName, errCode);
    		errors.add(error);
    	}
    	
    	List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
    	for(ObjectError globalError : globalErrors) {
    		String entityName = globalError.getObjectName();
    		String fieldName = globalError.getCode();
    		String errCode = globalError.getDefaultMessage();
    		error = ValidationError.ofFieldType(entityName, fieldName, errCode);
    		errors.add(error);
    	}
    	ApiErrors<ValidationError> apiErrors = new ApiErrors<>(errors, status.value(), getRequestURI());
    	return new ResponseEntity<>(apiErrors,status);
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
    	List<MessageNotReadableError> errors = new ArrayList<>();
    	errors.add(new MessageNotReadableError(ex.getMessage()));
    	ApiErrors<MessageNotReadableError> apiErrors = new ApiErrors<>(errors, status.value(), getRequestURI());
    	return new ResponseEntity<>(apiErrors,status);
    }
    
    
    /*Methode pour recuperer Toute les autres erreurs */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
    		HttpStatus status, WebRequest request) {
    	// TODO Auto-generated method stub
    	return super.handleExceptionInternal(ex, body, headers, status, request);
    }
    
}
