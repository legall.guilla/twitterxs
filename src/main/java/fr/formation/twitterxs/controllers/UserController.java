package fr.formation.twitterxs.controllers;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.twitterxs.dtos.UserCreateDto;
import fr.formation.twitterxs.dtos.UserDto;
import fr.formation.twitterxs.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController{

	@Autowired
	private UserService service;
	
	@GetMapping("/list/{name}")
	public List<UserDto> listByName(@PathVariable("name") String name){
		
		return service.listByName(name);
	}
	
	@GetMapping("/list")
	public List<UserDto> list(){
		return service.list();
	}
	
	@PostMapping("/save")
	public Long save(@Valid @RequestBody UserCreateDto dto) {
		return service.save(dto);
	}
	
	@DeleteMapping("/delete/{id}")
	public Boolean delete(@PathVariable("id") Long id) {
		return service.delete(id);
	}
}
