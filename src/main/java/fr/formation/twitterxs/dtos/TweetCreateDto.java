package fr.formation.twitterxs.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class TweetCreateDto {

	private Long authorId;
	
	@NotEmpty(message = "{E_NOT_EMPTY}")
	@Size(max=280 , message = "{E_MAX_LENGTH_EXCEEDED}")
	private String content;
	
	public TweetCreateDto() {
	}
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
