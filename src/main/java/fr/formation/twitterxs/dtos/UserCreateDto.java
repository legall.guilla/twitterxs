package fr.formation.twitterxs.dtos;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import fr.formation.twitterxs.errors.validators.Adult;
import fr.formation.twitterxs.errors.validators.UniqueEmail;

public class UserCreateDto {

	private long id;
	
	@NotBlank(message = "{E_NOT_BLANK}")
	@Size(max=100 , message = "{E_MAX_LENGTH_EXCEEDED}")
	private String firstname;
	
	@NotBlank(message = "{E_NOT_BLANK}")
	@Size(max=100 , message = "{E_MAX_LENGTH_EXCEEDED}")
	private String lastname;
	
	@NotBlank(message = "{E_NOT_BLANK}")
	@Size(max=255 , message = "{E_MAX_LENGTH_EXCEEDED}")
	@Email(message = "{E_EMAIL_MALFORMED}")
	@UniqueEmail
	private String email;

	@NotNull(message = "{E_NOT_NULL}")
	@Past
	@Adult(age = 21)
	private LocalDate birthDate;
	
	@NotNull(message = "{E_NOT_NULL}")
	private long regionId;
	
	@Valid
	@NotNull
	private UserSecurityCreateDto userSecurity;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public UserCreateDto() {
		super();
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public long getRegionId() {
		return regionId;
	}
	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}
	public UserSecurityCreateDto getUserSecurity() {
		return userSecurity;
	}
	public void setUserSecurity(UserSecurityCreateDto userSecurity) {
		this.userSecurity = userSecurity;
	}

	
}
