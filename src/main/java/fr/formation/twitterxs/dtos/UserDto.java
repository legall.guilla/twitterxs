package fr.formation.twitterxs.dtos;

public class UserDto {

	private long id;
	private String firstName;
	private String lastName;
	private String country;
	
	public UserDto() {
		
	}
	
	public UserDto(long id, String firstName, String lastName, String country) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
