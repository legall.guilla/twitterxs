package fr.formation.twitterxs.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserSecurityCreateDto {

	@NotBlank(message = "{E_NOT_BLANK}")
	@Size(max=50 , message = "{E_MAX_LENGTH_EXCEEDED}")
	private String username;
	
	@NotBlank(message = "{E_NOT_BLANK}")
	@Size(max=10 , message = "{E_MAX_LENGTH_EXCEEDED}")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
