package fr.formation.twitterxs.dto;

/**
 * @author legal
 *
 */
public class TestDto {

	private String abbr;

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	@Override
	public String toString() {
		return "TestDto [abbr=" + abbr + "]";
	}
	
}
